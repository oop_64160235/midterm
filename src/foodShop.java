public class foodShop {
    
    private String name;
    private int menuChoice;
    private int choiceRes;
    private int wallet;
    private int price = 0;
    public foodShop(String name ,int choiceRes,int menuChoice,int wallet){
        this.name = name;
        this.choiceRes = choiceRes;
        this.menuChoice = menuChoice;
        this.wallet = wallet;
    }

    public int getPrice() {
        return price;
    }

    public String getName(){
        return name;
    }
    public int menuChoice(){
        return menuChoice;
    }
    public int choiceRes(){
        return choiceRes;
    }

    public int wallet(){
        return wallet;
    }
    public void Restaurant(){
        if (choiceRes == 1){
            System.out.println(name + " You had selected Uncle Curly Hair Steak");
            System.out.println();
            if (menuChoice == 1){
                System.out.println(name +" You had selected a ChickenSuperBurger 109 baht");
                System.out.println();
                price = 109 ;
            }
            else if (menuChoice == 2){
                System.out.println(name + " You had selected a Spaqhetti Cabonara 89 baht");
                System.out.println();
                price = 89 ;
            }
        }
        else if (choiceRes == 2){
            System.out.println(name + " You had selected Boat Noodle Shop");
            System.out.println();
            if (menuChoice == 1){
                System.out.println(name + " You had selected a Beef Noodle 55 Baht");
                System.out.println();
                price = 55;
            }
            else if (menuChoice == 2){
                System.out.println(name +" You had selected a Chicken Noodle 45 Baht");
                System.out.println();
                price = 45;
            }
        }
    }
    public void print(){
        System.out.println("-----------------------------------------------");
        Restaurant();
        if (wallet < price){
            System.out.println("Not enough money");
        }else{
            System.out.println("Your total balance is "+(wallet)+" Baht");
        }
        System.out.println("-----------------------------------------------");
    }
}