public class DeliveryFee {
    private int price;
    private int distance;
    private int wallet;

    public DeliveryFee(int price, int distance, int wallet) {
        this.price = price;
        this.distance = distance;
        this.wallet = wallet;
    }

    public int getPrice() {
        return price;
    }

    public int getDistance() {
        return distance;
    }

    public int getWallet() {
        return wallet;
    }

    public void delifeeShow() {
        if (wallet < (price + (distance * 6))) {
            System.out.println("The price that you need to pay is " + (price + (distance * 6)) + " Baht");
            System.out.println("Not enough money");
        } else {
            System.out.println("The price that you need to pay is " + (price + (distance * 6)) + " Baht");
            System.out.println("Confirm , Your all balance is " + (wallet - (price + (distance * 6))) + " Baht ");
        }
    }
}
